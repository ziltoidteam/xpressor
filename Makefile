COMPILER = g++
LINKER = g++

COMPILER_OPTIONS = -Wextra -O3
COMPILE = $(COMPILER) $(COMPILER_OPTIONS)

LINK_OPTIONS =
LINK = $(LINKER) $(LINKER_OPTIONS)

all: main.o bitdata.o data.o argumentProcessor.o coder.o caesar.o rise.o huffman.o delta.o negp.o checker.o generator.o
	$(LINK) src/main.o src/bitdata.o src/data.o src/argumentProcessor.o src/coder.o src/crypt/caesar.o src/compression/huffman.o src/compression/rise.o src/transform/delta.o src/transform/negp.o src/test/checker.o src/test/generator.o -o xpr

main.o: src/main.cpp
	$(COMPILE) -c src/main.cpp -o src/main.o

data.o: src/data.cpp
	$(COMPILE) -c src/data.cpp -o src/data.o

argumentProcessor.o: src/argumentProcessor.cpp
	$(COMPILE) -c src/argumentProcessor.cpp -o src/argumentProcessor.o

bitdata.o: src/bitdata.cpp
	$(COMPILE) -c src/bitdata.cpp -o src/bitdata.o

coder.o: src/coder.cpp
	$(COMPILE) -c src/coder.cpp -o src/coder.o

caesar.o: src/crypt/caesar.cpp
	$(COMPILE) -c src/crypt/caesar.cpp -o src/crypt/caesar.o

rise.o: src/compression/rise.cpp
	$(COMPILE) -c src/compression/rise.cpp -o src/compression/rise.o

huffman.o: src/compression/huffman.cpp
	$(COMPILE) -c src/compression/huffman.cpp -o src/compression/huffman.o

delta.o: src/transform/delta.cpp
	$(COMPILE) -c src/transform/delta.cpp -o src/transform/delta.o

negp.o: src/transform/negp.cpp
	$(COMPILE) -c src/transform/negp.cpp -o src/transform/negp.o

checker.o: src/test/checker.cpp
	$(COMPILE) -c src/test/checker.cpp -o src/test/checker.o

generator.o: src/test/generator.cpp
	$(COMPILE) -c src/test/generator.cpp -o src/test/generator.o

clean:
	find . -name "*.o" -delete

test: all
	./xpr test huffman

edit:
	git ls-files | xargs ${EDITOR} &
