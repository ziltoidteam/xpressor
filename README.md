# Readme

xPressor is a test compression program for learning and fun.

It uses some popular coding, compression and crypt algorithm and gets way to combine it.

## Short usage guide:

### 1. Compression:
Use 
```
xpr encode {algorithm} {parameters} [input-file] [output-file]
```


As example:
```
xpr encode rise 4 ./my_music.mp3 ./my_music.mp3.xpr
```

### 2. Decompression:
Use 
```
xpr decode {algorithm} {parameters} [input-file] [output-file]
```

As example:
```
xpr decode rise 4 ./my_music.mp3.xpr ./my_music.mp3
```

### 3. Standart input/output:
You also can skip input and/or output files and xpr will get/sent data from/to terminal:

```
xpr encode {algorithm} - -
xpr encode {algorithm}
```

You also can get all data from file and print it in terminal:
```
xpr decode {algorithm} MyData.xpr
```

And you can get all data from terminal and send it to file:
```
xpr encode {algorithm} - Saved.xpr
```

As example:
```
xpr encode delta ./my_music.mp3 | xpr encode rise 4 - ./my_music.mp3.xpr
```

### 4. Algorithm list:
Now xpr supports this algorithms:

    Coding:
    - Delta coding
    - Negp coding

    Crypt:
    - Caesar crypt {shift}

    Compression:
    - Rise coding {key}
    - Huffman coding

### 5. Testing
For run custom test-cases on concrete algorithm use:
```
xpr test {algorithm} {parameters}
```

As example:
```
xpr test huffman
```