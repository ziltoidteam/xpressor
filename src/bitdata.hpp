#ifndef BITDATA_H
#define BITDATA_H

#include "data.hpp"

#include <limits.h>
#include <list>

class BitData {
public:
    BitData(Data &data);

    bool end() const;

    bool read();
    void write(bool bit);

    std::list<bool> readBits(size_t count);
    void writeBits(const std::list<bool> &container);

    template<typename T> T readValue();
    template<typename T> void writeValue(T c);

    void flush();

private:
    Data &data;

    unsigned char readByte, writeByte;
    unsigned char readBitMask, writeBitMask;
};

template<typename T>
T BitData::readValue() {
    const size_t Tbits = sizeof(T) * CHAR_BIT;

    T result(0);
    for(size_t i = 0; i < Tbits; ++i) {
        const bool bit = read();
        result <<= T(1);
        if (bit) result |= T(1);
    }

    return result;
}

template<typename T>
void BitData::writeValue(T c) {
    const size_t Tbits = sizeof(T) * CHAR_BIT;
    const T maxLimit = T(1) << (Tbits - 1);

    for(size_t i = 0; i < Tbits; ++i) {
        write((c & maxLimit) == maxLimit);
        c <<= T(1);
    }
}

#endif /* BITDATA_H */
