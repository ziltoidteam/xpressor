#ifndef CODER_H
#define CODER_H

#include "data.hpp"

class Coder {
public:
    virtual void encode(Data &source, Data &destination) = 0;
    virtual void decode(Data &source, Data &destination) = 0;
};

#endif /* CODER_H */
