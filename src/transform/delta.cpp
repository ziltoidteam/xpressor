#include "delta.hpp"

Delta::Delta() {
    // Nope
}

void Delta::encode(Data &source, Data &destination) {
    unsigned char last = 0;

    while (!source.end()) {
        const unsigned char readed = source.read();
        const unsigned char delta = readed - last;

        destination.write(delta);
        last = readed;
    }
}

void Delta::decode(Data &source, Data &destination) {
    unsigned char last = 0;

    while (!source.end()) {
        const unsigned char readed = source.read();
        const unsigned char delta = readed + last;

        destination.write(delta);
        last = delta;
    }
}

