#ifndef NEGP_H
#define NEGP_H

#include "../coder.hpp"

class Negp: public Coder {
public:
    Negp();

    void encode(Data &source, Data &destination);
    void decode(Data &source, Data &destination);
};

#endif /* NEGP_H */
