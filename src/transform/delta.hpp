#ifndef DELTA_H
#define DELTA_H

#include "../coder.hpp"

class Delta: public Coder {
public:
    Delta();

    void encode(Data &source, Data &destination);
    void decode(Data &source, Data &destination);
};

#endif /* DELTA_H */
