#include "negp.hpp"

Negp::Negp() {
    // Nope
}

void Negp::encode(Data &source, Data &destination) {
    while (!source.end()) {
        const unsigned char readed = source.read();

        unsigned char negp;
        if (!(readed & 128)) { // Positive
            negp = readed * 2;
        } else { // Negative
            negp = readed * (-2) - 1;
        }

        destination.write(negp);
    }
}

void Negp::decode(Data &source, Data &destination) {
    while (!source.end()) {
        const unsigned char readed = source.read();

        unsigned char posn;
        if (!(readed & 1)) { // Even
            posn = readed / 2;
        } else { // Odd
            posn = (readed + 1) / (-2);
        }

        destination.write(posn);
    }
}

