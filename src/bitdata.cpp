#include "bitdata.hpp"

const unsigned char leftBitMask  = (unsigned char)(1) << (CHAR_BIT - 1);

BitData::BitData(Data &data)
: data(data) {
    readByte = writeByte = 0;

    readBitMask = leftBitMask;
    writeBitMask = leftBitMask;
}

bool BitData::end() const {
    return readBitMask == leftBitMask && data.end();
}

bool BitData::read() {
    if (readBitMask == leftBitMask) readByte = data.read();

    const bool result = readByte & readBitMask;
    readBitMask >>= 1;

    if (readBitMask == 0) readBitMask = leftBitMask;

    return result;
}

void BitData::write(bool bit) {
    if (bit) writeByte |= writeBitMask;
    writeBitMask >>= 1;

    if (writeBitMask == 0) {
        data.write(writeByte);
        writeByte = 0;
        writeBitMask = leftBitMask;
    }
}

void BitData::flush() {
    if (writeBitMask != leftBitMask) data.write(writeByte);
}

std::list<bool> BitData::readBits(size_t count) {
    std::list<bool> result;

    for(size_t i = 0; i < count; ++i) {
        bool bit = this->read();
        result.push_back(bit);
    }

    return result;
}

void BitData::writeBits(const std::list<bool> &container) {
    std::list<bool>::const_iterator it;
    for(it = container.begin(); it != container.end(); ++it) {
        this->write(*it);
    }
}
