#include "generator.hpp"

#include <stdlib.h>

const unsigned char minChar = 0x00;
const unsigned char maxChar = 0xFF;
const unsigned char averageCharDelta = 0xFF;

Data Generator::empty() {
    return Data();
}

Data Generator::filledWithRange(unsigned int size, unsigned char min, unsigned char max) {
    Data result;

    for (unsigned int i = 0; i < size; ++i) {
        unsigned char value = min + rand() % (max - min + 1);
        result.write(value);
    }

    return result;
}

Data Generator::filledWith(unsigned int size, unsigned char value) {
    return filledWithRange(size, value, value);
}

Data Generator::zeros(unsigned int size) {
    return filledWith(size, 0);
}

Data Generator::smalls(unsigned int size) {
    return filledWithRange(size, minChar, minChar + averageCharDelta);
}

Data Generator::averages(unsigned int size) {
    return filledWithRange(size, minChar + averageCharDelta, maxChar - averageCharDelta);
}

Data Generator::bigs(unsigned int size) {
    return filledWithRange(size, maxChar - averageCharDelta, maxChar);
}

Data Generator::ones(unsigned int size) {
    return filledWith(size, maxChar);
}

Data Generator::randoms(unsigned int size) {
    return filledWithRange(size, minChar, maxChar);
}
