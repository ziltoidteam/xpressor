#ifndef GENERATOR_H
#define GENERATOR_H

#include "../data.hpp"

class Generator {
public:
    static Data empty();

    static Data filledWith(unsigned int size, unsigned char value);
    static Data filledWithRange(unsigned int size, unsigned char min, unsigned char max);

    static Data zeros(unsigned int size);
    static Data smalls(unsigned int size);
    static Data averages(unsigned int size);
    static Data bigs(unsigned int size);
    static Data ones(unsigned int size);
    static Data randoms(unsigned int size);
};

#endif /* GENERATOR_H */
