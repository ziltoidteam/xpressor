#include "checker.hpp"

#include "generator.hpp"

#include <iostream>
#include <sstream>

void Checker::checkWithData(Coder* coder, Data &data, bool verboseMode) {
    Data originalData = data;


    if (verboseMode) std::clog << "      Encoding..." << std::endl;
    Data encodedData;
    coder->encode(data, encodedData);

    if (verboseMode) std::clog << "      Decoding..." << std::endl;
    Data decodedData;
    coder->decode(encodedData, decodedData);

    if (verboseMode) std::clog << "      Checking..." << std::endl;
    bool testPassed = true;
    while (!originalData.end() || !decodedData.end()) {
        if (originalData.end() != decodedData.end()) {
            testPassed = false;
            break;
        }

        unsigned char original = originalData.read();
        unsigned char decoded = decodedData.read();

        if (original != decoded) {
            testPassed = false;
            break;
        }
    }

    if (testPassed) {
        if (verboseMode) std::clog << "    Test was passed." << std::endl;
    } else {
        std::clog << "!!! Test failed." << std::endl;
    }
}

void Checker::complexCheck(Coder* coder, bool verboseMode) {
    const unsigned int size = 10000; // 10K

    if (verboseMode) std::clog << "Testing a coder..." << std::endl;

    Data empty = Generator::empty();
    if (verboseMode) std::clog << "  Testing on empty data..." << std::endl;
    checkWithData(coder, empty, verboseMode);

    Data zeros = Generator::zeros(size);
    if (verboseMode) std::clog << "  Testing on zeros data..." << std::endl;
    checkWithData(coder, zeros, verboseMode);

    Data smalls = Generator::smalls(size);
    if (verboseMode) std::clog << "  Testing on smalls data..." << std::endl;
    checkWithData(coder, smalls, verboseMode);

    Data averages = Generator::averages(size);
    if (verboseMode) std::clog << "  Testing on averages data..." << std::endl;
    checkWithData(coder, averages, verboseMode);

    Data bigs = Generator::bigs(size);
    if (verboseMode) std::clog << "  Testing on bigs data..." << std::endl;
    checkWithData(coder, bigs, verboseMode);

    Data ones = Generator::ones(size);
    if (verboseMode) std::clog << "  Testing on ones data..." << std::endl;
    checkWithData(coder, ones, verboseMode);

    Data randoms = Generator::randoms(size);
    if (verboseMode) std::clog << "  Testing on random data..." << std::endl;
    checkWithData(coder, randoms, verboseMode);

}
