#ifndef CHECKER_H
#define CHECKER_H

#include "../coder.hpp"
#include "../data.hpp"

class Checker {
public:
    static void checkWithData(Coder* coder, Data &data, bool verboseMode = false);
    static void complexCheck(Coder* coder, bool verboseMode = false);
};

#endif /* CHECKER_H */
