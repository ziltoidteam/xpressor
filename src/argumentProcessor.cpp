#include "argumentProcessor.hpp"

#include <iostream>
#include <fstream>
#include <stdlib.h>

#include "coder.hpp"
#include "compression/huffman.hpp"
#include "compression/rise.hpp"
#include "crypt/caesar.hpp"
#include "transform/delta.hpp"
#include "transform/negp.hpp"

ArgumentProcessor::ArgumentProcessor(int argc, char **argv) {
    this->argc = argc;
    this->argv = argv;
    this->index = 1;
}

void error(const std::string& description) {
    std::cerr << "Error: " << description << std::endl;
    exit(1);
}

bool ArgumentProcessor::argumentsEnd() {
    return (index >= argc);
}

std::string ArgumentProcessor::getString() {
    if( argumentsEnd() ) error("not enougth parameters");

    return argv[index++];
}

Mode::Type ArgumentProcessor::getMode() {
    const std::string &mode = getString();

    if (mode == "encode") {
        return Mode::Encode;
    } else if (mode == "decode") {
        return Mode::Decode;
    } else if (mode == "test") {
        return Mode::Test;
    } else {
        error("bad mode");
    }
}

int ArgumentProcessor::getNumber() {
    const std::string &asText = getString();

    return atoi(asText.c_str());
}

int ArgumentProcessor::getNumber(int lower, int highter) {
    const int result = getNumber();

    if (lower <= result && result < highter) {
        return result;
    } else {
        error("bad argument value");
    }
}

Coder* ArgumentProcessor::getCoder() {
    const std::string &algorithm = getString();

    if(algorithm == "rise") {
        const unsigned char key = getNumber(0, 8);
        return new Rise(key);
    } else if(algorithm == "huffman") {
        return new Huffman();
    } else if(algorithm == "delta") {
        return new Delta();
    } else if(algorithm == "negp") {
        return new Negp();
    } else if(algorithm == "caesar") {
        const unsigned char key = getNumber();
        return new Caesar(key);
    } else error("unknown algorithm");
}

std::istream* ArgumentProcessor::getInputStream() {
    if (argumentsEnd())
        return &std::cin;

    const std::string &fileName = getString();

    if(fileName == "-")
        return &std::cin;

    return new std::ifstream(fileName.c_str(), std::ios::binary | std::ios::in);
}

std::ostream* ArgumentProcessor::getOutputStream() {
    if (argumentsEnd())
        return &std::cout;

    const std::string &fileName = getString();

    if( fileName == "-")
        return &std::cout;

    return new std::ofstream(fileName.c_str(), std::ios::binary | std::ios::out);
}
