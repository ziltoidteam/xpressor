#ifndef ARGUMENT_PROCESSOR_H
#define ARGUMENT_PROCESSOR_H

#include <iostream>
#include <string>
#include "coder.hpp"

namespace Mode {
    enum Type {Test, Encode, Decode};
}

class ArgumentProcessor {
public:
    ArgumentProcessor(int argc, char **argv);

    Coder* getCoder();
    std::istream* getInputStream();
    std::ostream* getOutputStream();

    Mode::Type getMode();

private:
    int argc;
    char **argv;
    int index;

    int getNumber();
    int getNumber(int lower, int highter);

    bool argumentsEnd();
    std::string getString();
};

#endif /* ARGUMENT_PROCESSOR_H */
