#include "argumentProcessor.hpp"
#include "test/checker.hpp"

void processArguments(int argc, char **argv) {
    ArgumentProcessor processor(argc, argv);

    Mode::Type mode = processor.getMode();
    Coder *coder = processor.getCoder();

    if(mode == Mode::Test) {
        Checker::complexCheck(coder, true);
        return ;
    }

    std::istream *inputStream = processor.getInputStream();
    std::ostream *outputStream = processor.getOutputStream();

    Data input;
    Data output;

    input.loadFromStream(*inputStream);
    if (mode == Mode::Encode) {
        coder->encode(input, output);
    } else if (mode == Mode::Decode){
        coder->decode(input, output);
    }
    output.saveToStream(*outputStream);
}

int main(int argc, char **argv) {
    processArguments(argc, argv);

    return 0;
}
