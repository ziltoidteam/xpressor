#ifndef RISE_H
#define RISE_H

#include "../coder.hpp"

class Rise: public Coder {
public:
    Rise(unsigned char k = 0);

    void encode(Data &source, Data &destination);
    void decode(Data &source, Data &destination);

private:
    unsigned char k;
    unsigned char m;
};

#endif /* RISE_H */
