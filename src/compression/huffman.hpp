#ifndef HUFFMAN_H
#define HUFFMAN_H

#include "../coder.hpp"

class Huffman: public Coder {
public:
    Huffman();

    void encode(Data &source, Data &destination);
    void decode(Data &source, Data &destination);
};

#endif /* HUFFMAN_H */
