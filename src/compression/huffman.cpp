#include "huffman.hpp"

#include <iostream>
#include <list>
#include <map>
#include <tr1/memory>

#include "../bitdata.hpp"



struct HuffmanTree {
    typedef std::tr1::shared_ptr<HuffmanTree> Pointer;

    HuffmanTree(unsigned long long value
              , unsigned char ch
              , const Pointer left = Pointer()
              , const Pointer right = Pointer());


    void buildTable(std::vector<std::list<bool> > &table) const;
    unsigned int depth() const;
    bool isLeaf() const;

    bool operator<(const HuffmanTree& rhs) const;

    unsigned char ch;
    unsigned long long value;
private:
    Pointer left;
    Pointer right;
    
    void buildCodes(std::list<bool>& list, std::vector<std::list<bool> > &table) const; 
};

HuffmanTree::HuffmanTree(unsigned long long value
          , unsigned char ch
          , const Pointer left
          , const Pointer right)
{
    this->value = value;
    this->ch = ch;
    this->left = left;
    this->right = right;
}

bool HuffmanTree::operator<(const HuffmanTree& rhs) const {
    return (this->value < rhs.value);
}

bool HuffmanTree::isLeaf() const {
    return (!left && !right);
}

unsigned int HuffmanTree::depth() const {
    if (isLeaf()) return 1;

    unsigned int leftDepth = this->left? left->depth(): 0;
    unsigned int rightDepth = this->right? right->depth(): 0;

    return 1 + std::max(leftDepth, rightDepth);
}

void HuffmanTree::buildCodes(std::list<bool>& list, std::vector<std::list<bool> > &table) const {
    if(isLeaf()) {
        table[ch] = list;
        return;
    }

    if(left) {
        list.push_back(false);
        left->buildCodes(list, table);
        list.pop_back();
    }
    
    if(right) {
        list.push_back(true);
        right->buildCodes(list, table);
        list.pop_back();
    }
}

// This algorithm should be called on root tree element only.
void HuffmanTree::buildTable(std::vector<std::list<bool> > &table) const {
    std::list<bool> list;

    /* There is very special case when root is a leaf. In this case 
     * we build symbol table manually 
     */
    if(this->isLeaf()) {
        list.push_back(true);
        table[ch] = list;
        return ;
    }
        
    buildCodes(list, table);
}

struct MetaData{
    MetaData(const std::vector<std::list<bool> > &tableValue, unsigned long long dataSizeValue, unsigned int tableSizeValue);

    const std::vector<std::list<bool> > table;
    const unsigned long long dataSize;
    const unsigned int tableSize;
};

MetaData::MetaData(const std::vector<std::list<bool> > &tableValue, unsigned long long dataSizeValue, unsigned int tableSizeValue)
    :table(tableValue), dataSize(dataSizeValue), tableSize(tableSizeValue) {
    // None
}



Huffman::Huffman() {
    // None
}

HuffmanTree::Pointer removeAndGetPoorest(std::list<HuffmanTree::Pointer> &fTable) {
    if(fTable.begin() == fTable.end())
        throw 1;

    std::list<HuffmanTree::Pointer>::iterator minit = fTable.begin();
    std::list<HuffmanTree::Pointer>::iterator it;

    for( it = fTable.begin(); it != fTable.end(); ++it)
        if((*it)->value < (*minit)->value)
            minit = it;

    HuffmanTree::Pointer result = *minit;
    fTable.erase(minit);

    return result;
}

void uniteAndAddPoorest(std::list<HuffmanTree::Pointer> &fTable) {
    const HuffmanTree::Pointer poorest1 = removeAndGetPoorest(fTable);
    const HuffmanTree::Pointer poorest2 = removeAndGetPoorest(fTable);

    const HuffmanTree::Pointer united(
        new HuffmanTree(poorest1->value + poorest2->value, 0, poorest1, poorest2)
    );

    fTable.push_back(united);
}

void increaseElement(std::vector<unsigned long long> &fTable, unsigned char c) {
    ++fTable[c];
}


// Method restores the state of SOURCE to the initial state after reading
const MetaData getMetaData(Data &source) {
    std::vector<unsigned long long> fTable(256,0ull);

    Data source2;
    unsigned long long dataSize = 0;
    unsigned int tableSize = 0;
    std::vector<std::list<bool> > table(256);

    while (!source.end()) {
        ++dataSize;
        const unsigned char c = source.read();
        increaseElement(fTable, c);
        source2.write(c);
    }
    
    // empty alphabet
    if( dataSize == 0 ){
        return MetaData(table,dataSize, tableSize);
    }

    std::list<HuffmanTree::Pointer> trees;
    for(unsigned int i = 0; i < 256; ++i)
        if(fTable[i] > 0) {
            const unsigned char symbol = (unsigned char)i;
            const HuffmanTree::Pointer element(new HuffmanTree(fTable[i], symbol));
            trees.push_back(element);
        }

    while (trees.size() > 1) {
        uniteAndAddPoorest(trees);
    }

    const HuffmanTree::Pointer &tree = *(trees.begin());
    tree->buildTable(table);

    for( unsigned int i = 0; i < 256; ++i)
    {
        if(!table.at(i).empty())
            ++tableSize;
    }

    source = source2;

    return MetaData(table,dataSize, tableSize);
}

void Huffman::encode(Data &source, Data &destination) {
    const MetaData metaData = getMetaData(source);

    BitData destinationBits(destination);
    destinationBits.writeValue<unsigned long long>(metaData.dataSize);
    destinationBits.writeValue<unsigned int>(metaData.tableSize);

    for( unsigned int i = 0; i < metaData.table.size(); ++i ) {
        const unsigned char ch = (unsigned char) i;
        const std::list<bool> bits = metaData.table.at(i);

        if(bits.empty()) continue;

        destinationBits.writeValue<unsigned char>(ch);
        destinationBits.writeValue<unsigned char>(bits.size());
        destinationBits.writeBits(bits);
    }

    while(!source.end()) {
        const unsigned char c = source.read();
        const std::list<bool> &bits = metaData.table.at(c);

        destinationBits.writeBits(bits);
    }

    destinationBits.flush();
}

void Huffman::decode(Data &source, Data &destination) {
    BitData sourceBits(source);

    const unsigned long long dataSize = sourceBits.readValue<unsigned long long>();
    const unsigned int tableSize = sourceBits.readValue<unsigned int>();

    std::map<std::list<bool>, unsigned char> map;
    for( unsigned int i = 0; i < tableSize; ++i ) {
        const unsigned char ch = sourceBits.readValue<unsigned char>();
        const unsigned char bitsCount = sourceBits.readValue<unsigned char>();
        const std::list<bool> bits = sourceBits.readBits(bitsCount);
        map[bits] = ch;
    }

    std::list<bool> bits;
    unsigned long long count = 0;
    while(!sourceBits.end() && count < dataSize) {
        bool bit = sourceBits.read();
        bits.push_back(bit);
        std::map<std::list<bool>, unsigned char>::const_iterator it = map.find(bits);
        if(it != map.end()) {
            destination.write(it->second);
            bits.clear();
            ++count;
        }
    }

    if (!bits.empty ()) throw 1;
}
