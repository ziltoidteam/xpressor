#include "rise.hpp"

#include "../bitdata.hpp"

Rise::Rise(unsigned char k) {
    this->k = k;
    this->m = (unsigned char)(1) << k;
}

void Rise::encode(Data &source, Data &destination) {
    BitData outBites(destination);

    while (!source.end()) {
        const unsigned char byte = source.read();
        const unsigned char q = byte / m;

        for(unsigned char i = 0; i < q; ++i)
            outBites.write(false);
        outBites.write(true);

        unsigned char r = byte % m;
        for(unsigned char j = 0; j < k; ++j) {
            outBites.write(r % 2 != 0);
            r /= 2;
        }
    }

    outBites.flush();
}

void Rise::decode(Data &source, Data &destination) {
    BitData inBites(source);

    while (!inBites.end()) {
        unsigned char q;
        for (q = 0; !inBites.read(); ++q)
            if (inBites.end()) return;

        unsigned char r = 0;
        for(unsigned char j = 0; j < k; ++j)
            if (inBites.read()) r |= (1 << j);

        const unsigned char byte = q * m + r;
        destination.write(byte);
    }
}
