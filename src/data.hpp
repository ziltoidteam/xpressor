#ifndef DATA_H
#define DATA_H

#include <iostream>
#include <vector>

class Data {
public:
    Data();

    void loadFromStream(std::istream &input);
    void saveToStream(std::ostream &output);

    bool end() const;

    unsigned char read();
    void write(unsigned char);

private:
    void next();

    // @TODO: Replace vector to stream
    size_t dataIndex;
    std::vector<unsigned char> data;
};

#endif /* DATA_H */
