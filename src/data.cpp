#include "data.hpp"

Data::Data() {
    this->dataIndex = 0;
}

void Data::loadFromStream(std::istream &input) {
    char ch;
    while(input.get(ch))
        this->data.push_back((unsigned char)ch);
}

void Data::saveToStream(std::ostream &output) {
    while (!end()) {
        output << read();
    }

    output.flush();

    std::clog << "Saved " << this->data.size() << " bytes." << std::endl;
}

bool Data::end() const {
    return (this->dataIndex >= this->data.size());
}

unsigned char Data::read() {
    const unsigned char result = this->data[dataIndex];
    next();
    return result;
}

void Data::write(unsigned char value) {
    this->data.push_back(value);
}

void Data::next() {
    if (end()) throw 1;

    ++dataIndex;
}
