#include "caesar.hpp"

Caesar::Caesar(unsigned char key)
: key(key) {
    // None
}

void Caesar::encode(Data &source, Data &destination) {
    while (!source.end()) {
        const unsigned char readed = source.read();
        const int coded = int(readed) + int(key);
        const unsigned char codedChar = coded % 256;
        destination.write(coded);
    }
}

void Caesar::decode(Data &source, Data &destination) {
    while (!source.end()) {
        const unsigned char readed = source.read();
        const int coded = int(readed) - int(key);
        const unsigned char codedChar = coded % 256;
        destination.write(coded);
    }
}
