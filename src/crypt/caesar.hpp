#ifndef CAESAR_H
#define CAESAR_H

#include "../coder.hpp"

class Caesar: public Coder {
public:
    Caesar(unsigned char key);

    void encode(Data &source, Data &destination);
    void decode(Data &source, Data &destination);

private:
    const unsigned char key;
};

#endif /* CAESAR_H */
